# Générateur de flux RSS non tronqué de Télérama

Le flux RSS de Télérama est modifié pour contenir les articles en entier et non de manière tronquée.

La modification du flux nécessite un compte et un [abonnement valide](https://abo.telerama.fr/).

## Dépendances

```
apt install libmojolicious-perl libxml-rss-perl
```

## Configuration

La configuration est gérée par trois variables d’environnement :
- `TELERAMA_RSSFILE` : chemin du fichier RSS à créer
- `TELERAMA_LOGIN` : l’email ou votre identifiant utilisé pour votre compte sur Télérama
- `TELERAMA_PWD` : le mot de passe de votre compte sur Télérama
- `TELERAMA_CATEGORY` : le flux que vous souhaitez modifier. Les choix valides sont `une`, `cinema`, `ecrans`, `enfants`, `sortir`, `musique`, `radio`, `livre` et `debats`.

## Installation

```
wget https://framagit.org/fiat-tux/rss/telerama-rss/-/raw/master/telerama-rss.pl -O /opt/telerama-rss.pl
chmod +x /opt/telerama-rss.pl
```

## Utilisation

```
TELERAMA_RSSFILE="/var/www/mon_flux_rss_des_articles_telerama.rss" TELERAMA_LOGIN="me@exemple.org" TELERAMA_PWD="foobarbaz" TELERAMA_CATEGORY="ecrans" /opt/telerama-rss.pl
```

## Cron

Voici un exemple de tâche cron pour mettre à jour le flux toutes les heures :
```
45 * * * * TELERAMA_RSSFILE="/var/www/mon_flux_rss_des_articles_telerama.rss" TELERAMA_LOGIN="me@exemple.org" TELERAMA_PWD="foobarbaz" TELERAMA_CATEGORY="ecrans" /opt/telerama-rss.pl
```

## Licence

Affero GPLv3. Voir le fichier [LICENSE](LICENSE).
